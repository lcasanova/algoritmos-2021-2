#include <iostream>
#include <cstdlib>
#include <ctime>
#include <cctype>

//EJERCICIO 1

using namespace std;

void imprimir (int b[], int n) {

 cout<< "El arreglo es: ";
 for(int i = 0; i<n; i++) 
        cout<<b[i]<<" ";
}

int main(){
 int n;
 int cuadrados=0;
 //preguntando el tamaño del arreglo
 cout << "Indique el tamano del arreglo: " << endl;
 cin>>n;
 
 int arreglo[n];

 srand(time(0));
 for(int i = 0; i<n; i++)
  arreglo[i] = 1 + rand()%5; // se rellena el arreglo con números aleatorios del 1 al 5 

 imprimir(arreglo, n); //imprime el arreglo sin cambios
 //cuadrado de los numeros
 for(int i = 0; i<n; i++)
  cuadrados=cuadrados+(arreglo[i]*arreglo[i]);
  
cout<<endl; 
cout<<"La suma de los cuadrados de los numeros aleatorios es: "<<cuadrados<<endl;
 //imprime el resultado final
 return 0;
}
